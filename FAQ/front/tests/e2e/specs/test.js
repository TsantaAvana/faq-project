// https://docs.cypress.io/api/introduction/api.html

describe("Test", () => {
    it("Visits Home page and expect", () => {
        cy.visit("/");
        cy.contains("h1", "Bienvenue sur Devngo");
    });
    it("Visits HomePage and verify that all the buttons are present", () => {
        cy.visit("/");
        cy.contains("button", "Poser des questions");
        cy.contains("button", "FAQ");
        cy.contains("button", "Répondre aux questions");
        cy.contains("button", "Home");
    });
    it("visits HomePage and verify if the button Poser des questions redirige vers ask-question", () => {
        cy.visit("/");
        cy.contains("button", "Poser des questions").click();
        cy.url().should("include", "/ask-questions");
    });
    it("visits askpage and verify button submit is disabled if any of the fields are empty", () => {
        cy.visit("/ask-questions");
        cy.contains("h1", "Posez des questions");
        cy.get(".form___container")
            .find("#title__field")
            .type("Pourquoi autant de produit defectueux?");
        cy.get(".form___container")
            .find("#description__field")
            .type("J ai un probleme avec mon achat!");
        cy.contains("Soumettre").should("be.disabled");
    });
    it("visits answer page and answer to question", () => {
        cy.visit("/answer-questions");
        cy.contains("h1", "Les listes des questions");
        cy.contains("Pourquoi on ne meurt pas?").click();
        cy.get(".response__form").contains("Repondre").click();

        cy.get(".modal__content").contains("Soumettre").should("be.disabled");

        cy.get(".modal__content")
            .find("#response__field")
            .type("Parce que nous sommes immortel");
        cy.get(".modal__content").contains("Soumettre").click();
    });
});
