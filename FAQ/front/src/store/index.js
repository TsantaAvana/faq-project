import Vue from "vue";
import Vuex from "vuex";
import Question from "./modules/question";
import Answer from "./modules/answer";
import ProblemType from "./modules/problemType";

Vue.use(Vuex);

export default new Vuex.Store({
    // state: {},
    // mutations: {},
    // actions: {},
    modules: {
        question: Question,
        answer: Answer,
        problem: ProblemType,
    },
});
