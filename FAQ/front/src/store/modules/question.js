import axios from "axios";
import { error_notification } from "../../plugins/notification";

export default {
    namespaced: true,
    state: {
        questions: [],
    },
    getters: {
        getQuestions: (state) => state.questions,
    },
    mutations: {
        SET_ALL_QUESTIONS: (state, payload) => (state.questions = [...payload]),
    },
    actions: {
        requestQuestions: async ({ commit }) => {
            try {
                const response = await axios({
                    url: `${process.env.VUE_APP_BACK_URL}/api/questions`,
                    method: "GET",
                });

                commit("SET_ALL_QUESTIONS", response.data);
            } catch (error) {
                console.log(`error`, error);
            }
        },
        createQuestion: async ({}, payload) => {
            try {
                console.log(`payload`, payload);
                await axios({
                    url: `${process.env.VUE_APP_BACK_URL}/api/questions`,
                    method: "POST",
                    data: {
                        ...payload,
                    },
                });
            } catch (error) {
                error_notification("Erreur de soumission");
            }
        },
    },
};
