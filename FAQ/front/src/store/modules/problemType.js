import axios from "axios";

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        requestProblemType: async () => {
            try {
                const response = await axios({
                    url: `${process.env.VUE_APP_BACK_URL}/api/problem-type`,
                    method: "GET",
                });
                return response.data;
            } catch (error) {
                console.log(`error`, error);
            }
        },
    },
};
