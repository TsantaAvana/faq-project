import axios from "axios";
import { error_notification } from "../../plugins/notification";
export default {
    namespaced: true,
    state: {
        answers: [],
    },
    getters: {
        getAnswers: (state) => state.answers,
    },
    mutations: {
        SET_ALL_ANSWERS: (state, payload) => (state.answers = [...payload]),
    },
    actions: {
        requestAnswers: async ({ commit }) => {
            try {
                const response = await axios({
                    url: `${process.env.VUE_APP_BACK_URL}/api/answers`,
                    method: "GET",
                });

                commit("SET_ALL_ANSWERS", response.data);
            } catch (error) {
                console.log(`error`, error);
            }
        },
        answerQuestion: async ({}, payload) => {
            try {
                await axios({
                    url: `${process.env.VUE_APP_BACK_URL}/api/answers`,
                    method: "POST",
                    data: {
                        ...payload,
                    },
                });
            } catch (error) {
                error_notification("Une erreur est survenue!");
            }
        },
    },
};
