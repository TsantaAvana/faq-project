import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import Toast, { POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";
import AxiosPlugin from "vue-axios-cors";

Vue.use(AxiosPlugin);
Vue.use(Vuetify);
const options = {
    position: POSITION.BOTTOM_RIGHT,
    draggable: false,
};
Vue.use(Toast, options);

export default new Vuetify({});
