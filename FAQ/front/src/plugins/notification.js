import Vue from "vue";

const success_notification = (message) => {
    Vue.$toast.success(message, {
        timeout: 5000,
    });
};

const error_notification = (message) => {
    Vue.$toast.error(message ?? "Oups, une erreur est survenue...", {
        timeout: 5000,
    });
};

export { success_notification, error_notification };
