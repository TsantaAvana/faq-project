from django.db import models

# Create your models here.
class Question (models.Model): 
    title= models.CharField(max_length=100, blank=1)
    problemID= models.IntegerField()
    description = models.CharField(max_length=100, blank=1)

class Answer (models.Model): 
    answer= models.CharField(max_length=100, blank=1)
    idquestion= models.IntegerField()

class ProblemType (models.Model): 
    description= models.CharField(max_length=100, blank=1)


def __str__(self):
    return self.title